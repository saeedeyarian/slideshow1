let slideIndex = 1;

slideShow (slideIndex);

function plusSlides (n) {
    slideShow (slideIndex += n)
}

function currentSlide (n) {
    slideShow(slideIndex = n)
}

function slideShow (n) {
    let images = document.getElementsByClassName ("image");
    let dots = document.getElementsByClassName ("dot");

    if (n > images.length) {
        slideIndex = 1;
    }
    if ( n < 1) {
        slideIndex = images.length
    }

    for (let i = 0; i < images.length ; i++) {
        images[i].style.display = "none";
    }
    for (let i = 0; i < dots.length ; i++) {
        dots[i].className = dots[i].className.replace (" active" , "");
    }

    images[slideIndex -1].style.display = "block";
    dots[slideIndex -1].className += " active";
}